#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

chmod a+x device/samsung/a10s/mkbootimg
add_lunch_combo omni_a10s-eng
